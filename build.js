const removeDirectory = require('@muffin-dev/node-helpers').removeDirectory;
const copyFileAsync = require('@muffin-dev/node-helpers').copyFileAsync;

(async () => {
  // Remove existing /doc directory
  await removeDirectory('./doc');

  // Copy original README.md file
  await copyFileAsync('./README.md', './doc/README.md');
})();
