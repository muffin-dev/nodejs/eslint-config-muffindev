# Muffin Dev for Node - ESLint Config

This module just exports a configuration preset for [ESLint](https://eslint.org) linting tool. This preset is used in most of the Muffin Dev's Node JS projects.

The rules configuration provided by this module are based on *eslint* "default" rules, *typescript* and *unicorn*. Note that you don't need to install other frameworks to get it work, only *eslint* module is required.

## Installation

### Basic installation

You need to install eslint and this module first:

```bash
npm i --save-dev eslint @muffin-dev/eslint-config
```

You can also use *unicorn* and *typescript* plugins:

```bash
npm i --save-dev eslint eslint-plugin-unicorn @typescript-eslint/eslint-plugin
```

As a shortcut, you can add the `lint` script in your *package.json*:

```json
{
    "scripts": {
        "lint": "eslint ./**/*.ts ./**/*.js",
    }
}
```

Create an *.eslintrc* file at the root of your project. This file is meant to contain the ESLint configuration. You must add this module to the `extends` options in order to use this preset:

```json
{
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": [
        "@muffin-dev/eslint-config"
    ]
}
```

### Recommended installation

Muffin Dev's projects usually use the [standard](https://github.com/standard/eslint-config-standard) preset, [Typescript](https://www.npmjs.com/package/@typescript-eslint/eslint-plugin) and [Unicorn](https://github.com/sindresorhus/eslint-plugin-unicorn) plugins for ESLint:

```bash
npm i --save-dev eslint @muffin-dev/eslint-config eslint-config-standard eslint-plugin-import eslint-plugin-node eslint-plugin-promise eslint-plugin-standard eslint-plugin-unicorn @typescript-eslint/eslint-plugin @typescript-eslint/parser
```

*.eslintrc* file at the root of the projects:

```json
{
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": [
        "standard",
        "plugin:unicorn/recommended",
        "plugin:@typescript-eslint/recommended",
        "@muffin-dev/eslint-config"
    ],
    "parser": "@typescript-eslint/parser",
    "plugins": [
        "@typescript-eslint"
    ]
}
```

*package.json* scripts:

```json
{
    "scripts": {
        "lint": "eslint ./src/**/*.ts",
    }
}
```